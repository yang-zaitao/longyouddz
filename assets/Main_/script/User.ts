import { _decorator, Component, Node } from 'cc';
import { Main } from '../../Main_/Main';
import { Util } from '../../main/lib/Util';
// import { indexOfNode } from '../lib/UISelect';
const { ccclass, property } = _decorator;

@ccclass('User')
export class User extends Component {
    @property(Node)
    leftTag: Node = null;//左边标签name
    @property(Node)
    rightbody: Node = null;//右边body内容
    tagMap: Map<typeof this.leftTag, typeof this.rightbody> = new Map();
    exit() { Main.ToggleUI("Home", this.node.name) }
    protected start(): void {
        const tog = new Util.Toggle(this.leftTag);
        tog.onTag(msg => {
            tog.onCentent(this.rightbody, msg);
        })
    }
    /**复制id功能 */
    cpyId(ent: any) {
        wx.showModal({
            title: '提示',
            content: '这是一个模态弹窗',
            success(res) {
                if (res.confirm) {
                    console.log('用户点击确定')
                } else if (res.cancel) {
                    console.log('用户点击取消')
                }
            }
        })
        console.log(ent);
    }
}

