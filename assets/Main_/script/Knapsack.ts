import { _decorator, Component, Node, instantiate, Prefab, Label, Sprite } from 'cc';
import { Main } from '../../Main_/Main';
import { Util } from '../../main/lib/Util';
import { Http, LoadUrlImg } from '../../main/lib/Net';
import { BagItem, BagList } from '../../main/lib/Type';
const { ccclass, property } = _decorator;

@ccclass('Knapsack')
export class Knapsack extends Component {
    @property(Node)
    LeftBtn: Node = null;
    @property(Node)
    Centent: Node = null;
    @property(Prefab)
    TagBtn: Prefab = null;
    @property(Prefab)
    GardItem: Prefab = null;
    GardData: BagItem[] = null;
    exit() { Main.ToggleUI("Home", this.node.name) }
    start() {
        new Http((suc: BagList) => {
            console.log("背包内容数据", suc);
            if (suc.code === 0 && suc.data !== null) {
                this.GardData = suc.data;
                this.init();
            } else {
                // new Util.Alert().ShowAlert({
                //     msg: '背包数据为空！',
                //     cancel: false,
                //     confrm: true,
                //     callfun: (arg, node) => {
                //         node.destroy();
                //     }
                // })
                console.log("背包数据为空！");
            }
        }).baglist();
    }
    init() {
        //筛选去重的按键key
        let btnarr = [];
        this.GardData.forEach(item => {
            if (btnarr.indexOf(item.name) < 0) {
                btnarr.push(item.name);
            }
        })
        // btnarr = [];
        const [GardContext, NotLabel] = this.Centent.children;
        GardContext.active = !(NotLabel.active = btnarr.length < 1);//暂无数据文本
        if (btnarr.length > 0) {
            //更具key添加按钮
            btnarr.forEach(name => {
                const btn = instantiate(this.TagBtn);
                btn.name = name;
                btn.getComponent(Label).string = name;
                btn.children[0].children[0].getComponent(Label).string = name;
                btn.children[0].name = name;
                this.LeftBtn.addChild(btn);
            });
            //绑定按钮事件
            const tog = new Util.Toggle(this.LeftBtn);
            tog.onTag(this.click.bind(this));
        }
    }
    click(key: string) {
        const keyData = this.GardData.filter(gard => gard.name === key);
        const [GardContext, NotLabel] = this.Centent.children;
        GardContext.active = !(NotLabel.active = keyData.length < 1);//暂无数据文本
        if (keyData.length > 0) {
            //添加卡片数据
            GardContext.removeAllChildren();//清空子组件
            keyData.forEach(gard => {
                GardContext.addChild(this.setGardData(gard));//添加对应卡片的数据
            })
        }
        console.log(key, keyData);
    }
    setGardData(gard_: BagItem): Node {
        const gard = instantiate(this.GardItem);
        const [icon, title, disc] = gard.children;
        new LoadUrlImg().getFrame(gard_.icon).then(frame => {
            icon.getComponent(Sprite).spriteFrame = frame;
        })
        title.getComponent(Label).string = gard_.name;
        disc.getComponent(Label).string = gard_.numbs.toString();
        return gard;
    }
}