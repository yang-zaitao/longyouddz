import { _decorator, Toggle, Component, Slider, director, Node, UITransform } from 'cc';
import { AudioConfig, AudioConfig_ } from '../../main/lib/StorageInfo';
import { Audio } from '../../main/lib/Audio';
import { Main } from '../../Main_/Main';
import { Rules } from './Rules';
import { WECHAT } from 'cc/env';
const { ccclass, property } = _decorator;

@ccclass('Setting')
export class Setting extends Component {
    @property(Toggle)
    musical_: Toggle = null;
    @property(Toggle)
    soundEffect_: Toggle = null;
    @property(Node)
    slider_: Node = null;
    exit() { Main.ToggleUI("Home", this.node.name) }//退出当前UI
    audioConfig: AudioConfig_;
    protected start(): void {
        // this.audioConfig = null;
        this.audioConfig = new AudioConfig().data;//拿到当前音乐配置的缓存
        this.musical_.isChecked = this.audioConfig.musical;//拿到存好的背景音乐
        this.soundEffect_.isChecked = this.audioConfig.soundEffect;//拿到存好的音效
        this.slider_.getComponent(Slider).progress = this.audioConfig.volume;//拿到存好的进度条按钮
        this.slider_.children[0].getComponent(UITransform).width = this.slider_.children[0].parent.getComponent(UITransform).width * this.audioConfig.volume;//进度条
    }
    down({ target }: { target: Node }) {
        console.log(target.name);
        switch (target.name) {
            case "规则": Rules.openRules(this.node.name); break;
            case "退出":
                if (WECHAT) {
                    wx.exitMiniProgram();
                }
                break;
        }
    }
    //音乐
    onmusical(ent: Toggle) {
        this.musical_.isChecked = ent.isChecked;
        this.audioConfig.musical = ent.isChecked;
        this.setAudio();
    }
    //音效
    onsoundEffect(ent2: Toggle) {
        this.soundEffect_.isChecked = ent2.isChecked;
        this.audioConfig.soundEffect = ent2.isChecked;
        this.setAudio();
    }
    //音量条
    Itembg2({ progress }: { progress: number }) {
        this.slider_.getComponent(Slider).progress = progress;//设置进度条按钮
        this.slider_.children[0].getComponent(UITransform).width =
            this.slider_.children[0].parent.getComponent(UITransform).width * progress;//进度条背景
        this.audioConfig.volume = progress;
        this.setAudio();
    }
    //把调好的音量设置上去
    setAudio() {
        new AudioConfig().setAudio(this.audioConfig);//存到本地配置
        const audio: Audio = director.getScene().getChildByName("Audio").getComponent(Audio);//拿到当前场景音乐节点
        audio.initAudio();//设置音乐
    }
}

