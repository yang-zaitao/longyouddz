import { _decorator, Color, Component, EditBox, instantiate, Label, Node, Prefab } from 'cc';
import { Http, MsgItem, SubmitListType } from '../../main/lib/Net';
import { Util } from '../../main/lib/Util';
import { BtnType } from '../../main/common/Alert';
const { ccclass, property } = _decorator;
@ccclass('Customer')
export class Customer extends Component {
    @property(Node)
    editbox: Node = null;
    @property(Node)
    content: Node = null;
    @property(Prefab)
    msg: Prefab = null;
    close() { this.node.destroy() }
    start() {
        new Http<SubmitListType>(suc => {
            // console.log(suc)
            if (suc.code === 0) {
                this.content.removeAllChildren();//清空消息节点子组件
                suc.data.forEach(item => {
                    this.content.addChild(this.createLabel(item));
                })
            }
        }).submitList();
    }
    submit() {
        const editbox = this.editbox.getComponent(EditBox);
        console.log(editbox.string)
        //如果输入的内容小于10个字就不给输入
        if (editbox.string.length >= 10) {
            new Http<{ code: string, data: object, msg: string }>(data => {
                new Util.Alert().ShowAlert({
                    msg: data.msg,
                    cancel: false,
                    confrm: true,
                    callfun: (type: BtnType, node: Node): void => {
                        this.node.destroy()
                        setTimeout(() => {
                            node.destroy()
                        }, 500);
                    }
                })
            }).submit({ Content: editbox.string });
        } else {
            new Util.Alert().ShowAlert({
                msg: '反馈消息文字不能低于10个字!',
                cancel: false,
                confrm: true,
                callfun: (arg: BtnType, node: Node): void => {
                    node.destroy();
                }
            });
        }
    }
    /**根据msg数据实例化一个文字节点 */
    createLabel(msg: MsgItem): Node {
        const msgLabel = instantiate(this.msg);
        const msgNode = msgLabel.getComponent(Label);
        let message = `${msg.uid}(${msg.CreatedAt}):${msg.content}`;
        if (msg.isback === 1) {
            msgNode.color = new Color(255, 0, 0, 255);
            message = `客服:(${msg.CreatedAt}):${msg.content}`;
        }
        msgNode.string = message;
        return msgLabel;
    }
}

