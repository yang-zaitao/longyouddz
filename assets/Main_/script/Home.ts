import { _decorator, Component, Node } from "cc";
import { AudioConfig, GameID, Gamer, RoomList, RoomType } from "../../main/lib/StorageInfo";
import { Http, RoomConfig } from "../../main/lib/Net";
import { Main } from '../../Main_/Main';
import { Loding } from "../../main/Loding/Loding";
import { Shopping } from "./Shopping";
import { Util } from "../../main/lib/Util";
import { Wechat } from "../../main/Market/Wechat";
import { WECHAT } from "cc/env";
import { RoomType__, Team } from "./Team";
import { GoldAmple } from "../../main/lib/Type";
const { ccclass, type, property } = _decorator;
export enum keyType {
  user = "头像信息",
  gold = "金币",
  drill = "钻石",
  setting = "设置",
  exit = "退出",
  shenmi = "神秘商店",
  welfare = "福利",
  ranking = "排位",
  race = "赛事",
  team_create = "创建房间",
  team_join = "加入房间",
  activity = "活动",
  classicmode = "经典模式",
  NotShuffle = "不换牌",//不洗牌
  list = "排行榜",
  email = "邮件",
  taks = "任务",
  knapsack = "背包",
  mall = "商城",
  fastStart = "快速开始",
  mahjong = "龙游麻将",
  customer = "客服",
  signIn = "签到",
  lottery = "幸运抽奖",
  alms = "救济金"
}

@ccclass("Home")
export class Home extends Component {
  static Gold: GoldAmple = GoldAmple.Ample;
  /**头像信息 */
  @type(Node)
  iconInfo: Node = null;
  /**金币钻石 */
  @type(Node)
  fund: Node = null;
  /**商城|活动|背包|任务|邮件 */
  @property({ group: { name: '商城|活动|背包|任务|邮件' }, type: [Node] })
  control: Node[] = new Array(5);
  protected start(): void {
    if (new Gamer().coin > 2000) { //如果金币大于两千就去掉Home的金币不足状态
      Home.Gold = GoldAmple.Ample;
    }
    if (Home.Gold === GoldAmple.noAmple) {
      Main.ToggleUI('Alms')
    }
    this.setinfo();
  }
  setinfo() {
    if (WECHAT) {//判断是否处于微信小程序
      this.control[0].active = true;
      // if (!Wechat.isiOS()) {//判断不是iOS环境
      // }
    } else {
      this.control[0].active = true;
    }
    //---------获取房间配置---------//
    new Http<RoomConfig>(suc => {
      if (suc.code === 0) {//请求成功！
        new RoomList().saveData(suc);
        // console.log('房间配置', suc);
      }
    }).roolist();
    //---------设置首页数据-----------//
    new AudioConfig().init();//初始化音乐配置
    //头像____用户名+id
    const [icon, name_id] = this.iconInfo.children;
    new Gamer().setInfo(icon, name_id.children[0], name_id.children[1]);
  }
  showinfo(char: string) {
    let title = char.concat("正在开发中，敬请期待！");
    if (char === '排位' || char === '赛事') {
      title = '限时开启敬请期待'
    }
    new Util.Alert().ShowAlert({
      msg: title,
      cancel: false,
      confrm: true,
      callfun: (arg, node) => {
        node.destroy();
      }
    })
  }
  keydown({ target }: { target: Node }) {
    // console.log(target.name);
    switch (target.name) {
      case keyType.alms:
        Main.ToggleUI("Alms")
        // this.showinfo(keyType.alms)
        break;
      case keyType.lottery:
        Main.ToggleUI("Lottery")
        // this.showinfo(keyType.lottery)
        break;
      case keyType.signIn:
        Main.ToggleUI("SignIn")
        // this.showinfo(keyType.setting)
        break;
      case keyType.activity:
        this.showinfo(keyType.activity)
        break;
      case keyType.customer:
        // this.showinfo(keyType.customer)
        Main.ToggleUI("Customer")
        break;
      case keyType.user:
        this.showinfo(keyType.user)
        // Main.ToggleUI("User", this.node.name);  
        break;
      case keyType.setting: Main.ToggleUI("Setting", this.node.name, false); break;
      case keyType.exit: break;
      case keyType.shenmi: break;
      case keyType.welfare:
        // Main.ToggleUI("welfare", this.node.name);
        break;
      case keyType.ranking:
        this.showinfo(keyType.ranking)
        break;
      case keyType.race:
        this.showinfo(keyType.race)
        break;
      case keyType.team_create:
        Team.page = RoomType__.Create;
        Main.ToggleUI('Team')
        break;
      case keyType.team_join:
        Team.page = RoomType__.Join;
        Main.ToggleUI('Team')
        break;
      case keyType.classicmode: Main.ToggleUI("ClassicMode", this.node.name); break;
      case keyType.NotShuffle: Main.ToggleUI("Noshuffle", this.node.name); break;
      case keyType.list:
        this.showinfo(keyType.list)
        // Main.ToggleUI("List", this.node.name);
        break;
      case keyType.email:
        this.showinfo(keyType.email)
        //Main.ToggleUI("Email", this.node.name);
        break;
      case keyType.taks:
        this.showinfo(keyType.taks)
        // Main.ToggleUI('Task');
        break;
      case keyType.knapsack:
        // this.showinfo(keyType.knapsack)
        Main.ToggleUI("Knapsack", this.node.name);
        break;
      case keyType.mall: Main.ToggleUI("Shopping", this.node.name); break;
      case keyType.gold: Shopping.ShowBtn(0, this.node); break; //金币按钮
      case keyType.drill: Shopping.ShowBtn(1, this.node); break; //砖石按钮
      case keyType.mahjong:
        console.log("打开麻将");
        new Wechat().openMahjong();
        break;
      case keyType.fastStart://快速开始
        const rooms = new RoomList().getRoomConfig(RoomType.classic)//拿到房间数据
          .type_list.filter(item => item.play_type === 1)[0]//过滤到当前页面名称的模式数据
          .rooms.filter(item => item.room_type !== 0)//排除组队房
        const gamer = new Gamer();
        const roomArr = rooms.filter(item => {
          return item.name !== '好友房' && (item.entermin <= gamer.coin && gamer.coin <= item.entermax) || (item.entermin < gamer.coin && item.entermax === 0);//大于最小小于最大金币的
        })
        // console.log('房间列表', roomArr);

        console.log('快速开始符合房间列表', roomArr);
        const roomcfg = roomArr[roomArr.length - 1];//拿到合适的最难的房间
        if (roomcfg !== undefined) {
          console.log(roomcfg);
          new GameID().saveData({ roomid: roomcfg.id, uid: gamer.uid, roomtitle: roomcfg.name, score: roomcfg.entermin, base_score: roomcfg.base_score });
          Loding.start_Loding("Game");
        } else { //提示领取救济金
          Main.ToggleUI('Alms');
        }
        break;
    }
  }
}