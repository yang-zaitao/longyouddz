import { _decorator, Component, Label, Node } from 'cc';
import { AlmsDetail, Http, SucType } from '../../main/lib/Net';
import { HTML5, WECHAT } from 'cc/env';
import { Util } from '../../main/lib/Util';
import { BtnType } from '../../main/common/Alert';
import { fund } from '../../main/common/fund';
import { Wechat } from '../../main/Market/Wechat';
const { ccclass, property } = _decorator;

enum ReceiveType {
    ordinary = '普通领取',
    special = '双倍领取'
}

@ccclass('Alms')
export class Alms extends Component {
    @property(Label)
    subTitle: Label;
    status: number = 0;
    exit() { this.node.destroy() };
    start() {
        this.init();
    }
    init() {
        new Http<SucType<AlmsDetail>>((suc) => {
            // console.log(this);
            this.subTitle.string = `可以领取${suc.data.srate}金币，今日还剩余${suc.data.residue_time}次领取机会，分享双倍领取`;
            // this.subTitle.string = `观看广告视频可以领取${suc.data.srate}金币,今日还剩余${suc.data.residue_time}次领取机会`;
            this.status = suc.data.status;
        }).AlmsDetail();
    }
    click({ target }: { target: Node }) {
        let bol: 0 | 1 = 0;
        switch (target.name) {
            case ReceiveType.ordinary://普通领取
                bol = 0;
                break;
            case ReceiveType.special://双倍领取
                if(WECHAT){
                    new Wechat().shar();
                }
                bol = 1;
                break;
        }
        new Http<SucType<{}>>(suc => {
            console.log('领取数据', suc);
            if (suc.code === 0) {
                this.init();
            }
            new Util.Alert().ShowAlert({
                msg: suc.msg,
                cancel: false,
                confrm: true,
                callfun: (type: BtnType, node: Node) => {
                    node.destroy();
                }
            })
            fund.loadFund();
        }).ReceiveAlms(bol);
    }
}

