import { _decorator, Component, Input, instantiate, Label, Node, Prefab, Sprite } from 'cc';
import { Http, LoadUrlImg, sign, signinType } from '../../main/lib/Net';
import { Util } from '../../main/lib/Util';
import { BtnType } from '../../main/common/Alert';
const { ccclass, property } = _decorator;

enum SigninType {
    /**不能签到 */
    notSingin,
    /**已经超时待签 */
    repairSingin,
    /**可以签到 */
    Singin,
    /**已经签到 */
    toSingin
}

@ccclass('Signin')
export class Signin extends Component {
    @property(Node)
    SiginNode: Node = null;
    @property(Prefab)
    card_pref: Prefab = null;
    @property(Label)
    signDay: Label = null;
    signData: sign[];
    exit() { this.node.destroy() };
    start() {
        this.init();
    }
    init() {
        new Http((suc: signinType) => {
            console.log(suc);
            if (suc.code === 0 && suc.data.list.length > 0) {
                this.setsignDay()
                this.signData = suc.data.list;
                this.setCand()
            }
        }).signin()
    }
    setCand() {
        this.SiginNode.removeAllChildren();
        this.signData.forEach(card => {
            const node = instantiate(this.card_pref)
            node.children[node.children.length - 1].getComponent(Label).string = String(card.day);
            new LoadUrlImg().getFrame(card.image).then(frame => {
                node.children[0].children[0].getComponent(Sprite).spriteFrame = frame;
            })
            node.children[0].children[1].children[0].getComponent(Label).string = card.name;
            node.children[card.status].active = true;
            node.on(Input.EventType.TOUCH_START, () => this.singnin(card, node));
            this.SiginNode.addChild(node);
        })
    }
    singnin(card: sign, cardNode: Node) {
        const type = card.status;
        switch (type) {
            case SigninType.Singin:
                new Http((suc: { code: number; msg: string }) => {
                    this.setsignDay()
                    new Util.Alert().ShowAlert({
                        msg: suc.code === 0 ? `${card.name}签到成功!` : suc.msg,
                        cancel: false,
                        confrm: true,
                        callfun: (type: BtnType, node: Node): void => {
                            node.destroy();
                            cardNode.children[SigninType.Singin].active = false;
                            cardNode.children[SigninType.toSingin].active = true;
                        }
                    })
                }).sendSignin({ day: card.day })
                break;
            case SigninType.repairSingin:
                new Util.Alert().ShowAlert({
                    msg: '后续可花费补签卡补签',
                    cancel: true,
                    confrm: true,
                    callfun: function (type: BtnType, node: Node): void {
                        node.destroy()
                    }
                })
                break;
        }
    }
    setsignDay() {
        new Http((suc: signinType) => {
            console.log(suc);
            if (suc.code === 0 && suc.data.list.length > 0) {
                this.signDay.string = `本月总计签到${suc.data.total}天，连续签到${suc.data.continuous}天`;
            }
        }).signin()
    }
}

