import { _decorator, Component, Node } from 'cc';
import { Main } from '../../Main_/Main';
import { Util } from '../../main/lib/Util';
const { ccclass, property } = _decorator;

@ccclass('Email')
export class Email extends Component {
    @property(Node)
    LeftBtn: Node = null;
    exit() { Main.ToggleUI("Home", this.node.name) };
    start() {
        const tog = new Util.Toggle(this.LeftBtn);
        tog.setBtnidx(1);
        tog.onTag(suc => {
            console.log("邮箱页面按下的按钮",suc);
        });
    }
}