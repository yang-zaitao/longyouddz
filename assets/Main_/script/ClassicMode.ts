import { GameID, Gamer, RoomList, RoomType } from '../../main/lib/StorageInfo';
import { _decorator, Component, Label, Node } from 'cc';
import { Loding } from '../../main/Loding/Loding';
import { Log, Util } from '../../main/lib/Util';
import { Main } from '../Main';
import { Rules } from './Rules';
import { Room_ } from '../../main/lib/Net';
import { BtnType } from '../../main/common/Alert';
import { Shopping } from './Shopping';
import { keyType } from './Home';
// import { Game } from '../../Game/Game';
const { ccclass, property } = _decorator;

@ccclass('ClassicMode')
export class ClassicMode extends Component {
    @property(Node)
    btns: Node = null;
    // @property(Node)
    // fund: Node = null;
    // @property(Alert)
    // alert: Alert = null;
    roomData: Room_[]
    exit() { Main.ToggleUI("Home", this.node.name) }

    protected start(): void {
        console.log(new RoomList().getRoomConfig(RoomType.classic));
        this.init();
    }
    /**拿到本地存着的游戏配置list设置到页面上 */
    init() {
        const roomTitle = this.node.name;
        const PlayType = {
            /**经典模式 */
            ClassicMode: 1,
            /**不换牌 */
            Noshuffle: 2,
            /**好友房 */
            FriendsRoom: 3
        }
        const rooms = new RoomList().getRoomConfig(RoomType.classic)//拿到房间数据
            .type_list.filter(item => item.play_type === PlayType[roomTitle])[0]//过滤到当前页面名称的模式数据
            .rooms.filter(item => item.room_type !== PlayType.FriendsRoom)//排除好友房
        this.roomData = rooms;
        const ModeGrad = this.btns.children.map(item => item);
        ModeGrad.forEach((item, i) => {//根据node子节点数组设置对应room配置
            const [titleNode, base_scoreNode, enterNode] = item.children;
            item.name = rooms[i].id.toString();
            titleNode.children[0].getComponent(Label).string = rooms[i].name;//设置标题文字
            base_scoreNode.children[1].getComponent(Label).string = rooms[i].base_score.toString();//设置低分文字
            enterNode.children[2].getComponent(Label).string = `${new Util.Format(rooms[i].entermin).toString()}-${new Util.Format(rooms[i].entermax).infinite()}`//设置分数差
        });
    }
    openShop({ target }: { target: Node }) {
        switch (target.name) {
            case keyType.gold: Shopping.ShowBtn(0, this.node); break; //金币按钮
            case keyType.drill: Shopping.ShowBtn(1, this.node); break; //砖石按钮
        }
    }
    openHelp() {//显示游戏帮助
        Rules.openRules(this.node.name);//把name传过去用于返回
    }
    ondown({ target }: { target: Node }) {//根据点击的模式打开对应的模式游戏
        let roomid = Number(target.name);//拿到游戏房间id 
        const uid = new Gamer().uid;//拿到用户id
        const score = Number(target.getChildByName("底分").children[1].getComponent(Label).string);//根据节点拿到底分
        if (score === null) return Log.error("没有拿到低分数据无法开局?");
        // console.log(new GameID().roomid, new GameID().uid);
        let romItem: Room_;
        this.roomData.forEach(item => {
            if (item.id === roomid) {//更具点击卡片的id查找房间对象数据
                romItem = item;
                new GameID().saveData({ roomid, uid, score: item.entermin, roomtitle: item.name, base_score: item.base_score });//拿到的游戏数据配置存到本地
            }
        });

        const cion = new Gamer().coin;
        if ((romItem.entermin < cion && cion < romItem.entermax) || ( romItem.entermin < cion && romItem.entermin > 0 && romItem.entermax === 0)) {//如果金币满足
            Loding.start_Loding("Game");
            return;
        }
        const roomArr = this.roomData.filter(item => {
            //最小资金<持有资金and持有资金<最大资金 or 最小资金<持有资金and最大资金===0相当于无限
            return (item.entermin < cion && cion < item.entermax) || (item.entermin < cion && item.entermax === 0);
        });
        // console.log(this.roomData);
        console.log(roomArr);
        let room = roomArr[roomArr.length - 1];
        // console.log(roomid);
        // console.log(room);
        if (room !== undefined) {
            new Util.Alert().ShowAlert({
                msg: `资金不满足，是否前往${room?.name}游玩`,
                cancel: true,
                confrm: true,
                callfun: (type: BtnType, node: Node) => {
                    if (type === BtnType.Confrm) {
                        roomid = room.id //设置房间id
                        new GameID().saveData({ roomid, uid, score: room.entermin, roomtitle: room.name, base_score: room.base_score });//拿到的游戏数据配置存到本地
                        Loding.start_Loding("Game");
                    }
                    node.destroy();
                }
            })
        } else {//提示领取救济金
            Main.ToggleUI('Alms');
        }
    }
}