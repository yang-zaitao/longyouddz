import { _decorator, Component, Input, instantiate, Label, Node, Prefab, Sprite } from 'cc';
import { Http, List, LoadUrlImg, SucType, TaskList } from '../../main/lib/Net';
import { Util } from '../../main/lib/Util';
import { BtnType } from '../../main/common/Alert';
const { ccclass, property } = _decorator;

enum BtnStatus {
    /**已经领取 */
    Claimed = -1,
    /**待完成 */
    NotFinish = 0,
    /**已完成 */
    Finish = 1
}

@ccclass('Task')
export class Task extends Component {
    //任务tabs
    @property(Node)
    TabsNode: Node = null;
    @property(Prefab)
    TabsItem: Prefab = null;
    //任务列表节点
    @property(Node)
    TaskConten: Node = null;
    @property(Prefab)
    TaskList: Prefab = null;
    TaskData: TaskList[] = [];
    exit() { this.node.destroy(); }
    start() {
        this.init();
        setTimeout(() => {
            this.setTabs(this.TaskData); //生成添加按钮
            new Util.Toggle(this.TabsNode).onTag(this.keydown.bind(this)) //绑定按钮事件
        }, 300);
    }
    init() {
        new Http<SucType<TaskList[]>>(async suc => {
            if (suc.code === 0) {
                // console.log(suc.data);
                this.TaskData = suc.data;//存一个全局数据
            }
        }).taskList()
    }
    /**监听按钮key值 */
    keydown(key: string) {
        // console.log(this.TaskData);
        if (this.TaskData !== undefined) {
            //更具key的值找到对应的任务列表数据
            this.TaskData.forEach(task => {
                if (key === task.name) {
                    this.setList(task.list);
                }
            })
        }
    }
    /**添加tabs按钮 */
    setTabs(tkList: TaskList[]) {
        this.TaskConten.removeAllChildren();//清空子节点
        tkList.forEach(taskItem => {
            const item = instantiate(this.TabsItem);
            item.children[0].name = taskItem.name;//设置子节点的name属性否则
            item.getComponent(Label).string = taskItem.name; //设置没选中文字
            item.children[0].children[0].getComponent(Label).string = taskItem.name; //设置选中文字
            this.TabsNode.addChild(item);
        })
    }
    /**设置任务列表数据 */
    setList(TaskList: List[]) {
        this.TaskConten.removeAllChildren();//清空子节点
        TaskList.forEach(task => {
            const item = instantiate(this.TaskList);
            const [icon, titles, btns] = item.children;
            //设置任务列表图片
            new LoadUrlImg().getFrame('').then(frame => {
                icon.getComponent(Sprite).spriteFrame = frame;
            })
            //设置标题和副标题
            const [title, subtle] = titles.children;
            title.getComponent(Label).string = task.title;
            subtle.getComponent(Label).string = `${task.desc}(${task.user_expectant}/${task.expectant})`;
            //判断按钮状态并且绑定事件
            if (task.status === BtnStatus.NotFinish) {
                btns.children[BtnStatus.NotFinish].active = true;
            }
            if (task.status === BtnStatus.Claimed) {
                btns.children[btns.children.length + BtnStatus.Claimed].active = true;
                // item.getComponent(Sprite).grayscale = true;
            }
            if (task.status === BtnStatus.Finish) {
                btns.children[BtnStatus.Finish].active = true;
                btns.children[BtnStatus.Finish].on(Input.EventType.TOUCH_START, () => this.click(task, btns), task)
            }
            this.TaskConten.addChild(item);
        })
    }
    /**领取按钮点击事件 */
    click(task: List, btns: Node) {
        // console.log(task);
        // console.log(btns);
        new Http<SucType<{}>>(suc => {
            new Util.Alert().ShowAlert({
                msg: suc.msg,
                cancel: false,
                confrm: true,
                callfun: function (type: BtnType, node: Node): void {
                    node.destroy();
                }
            })
            if (suc.code === 0) {
                btns.children[BtnStatus.Finish].active = false;//隐藏领取按钮
                btns.children[btns.children.length + BtnStatus.Claimed].active = true;//显示已经领取
                this.init();
                setTimeout(() => {
                    new Util.Toggle(this.TabsNode).onTag(this.keydown.bind(this)) //绑定按钮事件
                }, 300);
            }
        }).receiveTask({
            id: task.id,
            item_id: task.item_id
        })
    }
}

