import { _decorator, Component, Input, instantiate, Label, Node, Prefab, Toggle } from 'cc';
import { GameID, Gamer, RoomList, RoomType as Room_Type } from '../../main/lib/StorageInfo';
import { Util } from '../../main/lib/Util';
import { Loding } from '../../main/Loding/Loding';
const { ccclass, property } = _decorator;

export enum RoomType__ {
    /**创建 */
    Create,
    /**加入 */
    Join
}

interface GameType {
    roomid: number,
    uid: number,
    score: number,
    base_score: number
    roomtitle: string,
}

// let page: RoomType = RoomType.Create;
// export const openPage = {
//     create: () => {
//         page = RoomType.Create;
//         Main.ToggleUI('Team')
//     },
//     join: () => {
//         page = RoomType.Join;
//         Main.ToggleUI('Team')
//     }
// }

export let Assets: { content: Node, context: Node, labe: Prefab, checkout: Prefab } = { content: undefined, context: undefined, labe: undefined, checkout: undefined }
@ccclass('Team')
export class Team extends Component {
    static page: RoomType__;
    /**创建房间节点 */
    @property(Node)
    CreateRoom: Node = null;
    /**加入房间节点 */
    @property(Node)
    JoinRoom: Node = null;
    @property(Node)
    Centent: Node = null;
    @property(Node)
    Context: Node = null;
    @property(Prefab)
    Item: Prefab = null;
    @property(Prefab)
    LabelText: Prefab = null;
    exit() { this.node.destroy() }
    start() {
        Assets = { content: this.Centent, context: this.Context, labe: this.LabelText, checkout: this.Item };
        switch (Team.page) {
            case RoomType__.Create:
                new CreateRoom(this.CreateRoom)
                break;
            case RoomType__.Join:
                new JoinRoom(this.JoinRoom)
                break;
        }
    }
    static TeamData = { create: false, join: false, roomid: undefined };
}


enum KeyDownType {
    Confirm = '确认',
    Delete = '删除'
}
/**创建房间 */
class CreateRoom {
    gameData: GameType = {
        roomid: 0,
        uid: 0,
        score: 0,
        base_score: 0,
        roomtitle: ''
    };
    constructor(create: Node) {
        create.active = true;//显示节点
        create.children[create.children.length - 1].on(Input.EventType.TOUCH_START, this.submit, this);
        const rooms = new RoomList().getRoomConfig(Room_Type.classic);
        //添加左边按钮列表
        rooms.type_list.forEach(item => {
            const txt = instantiate(Assets.labe);
            txt.children[0].name = item.name;
            txt.children[0].children[0].getComponent(Label).string = item.name;
            txt.getComponent(Label).string = item.name;
            Assets.content.addChild(txt);
        })
        //更具按钮列表设置右边body数据
        new Util.Toggle(Assets.content).onTag(key => {
            const room = rooms.type_list.filter(item => item.name === key)[0].rooms;
            Assets.context.removeAllChildren();
            room.forEach(item => {
                const checkout = instantiate(Assets.checkout);
                checkout.children[0].getComponent(Label).string = item.name;
                checkout.children[1].on(Input.EventType.TOUCH_START, this.click, this);
                if (item.name === '好友房') {
                    this.gameData.base_score = item.base_score;
                    this.gameData.roomid = item.id;
                    this.gameData.score = item.entermin;
                    this.gameData.roomtitle = item.name;
                    this.gameData.uid = new Gamer().uid;
                    const chk = checkout.children[1].getComponent(Toggle);
                    chk.isChecked = true;
                }
                Assets.context.addChild(checkout);
            });
        })
    }
    click({ target }: { target: Node }) {
        console.log(target.getComponent(Toggle).isChecked);
    }
    submit() {
        // console.log();
        new GameID().saveData(this.gameData);
        // Game.TeamData.create = true;
        // new TeamData().saveData({ create: true, join: false, roomid: undefined });
        Team.TeamData.create = true;
        Loding.start_Loding("Game");
    }
}
/**加入房间 */
class JoinRoom {
    /**输出内容节点 */
    OutNode: Node = null;
    /**输入节点 */
    InputNode: Node = null;
    /**输入的数组值 */
    valArr: Array<string> = [];
    constructor(join: Node) {
        console.log(join.children);
        join.active = true;//显示节点
        this.OutNode = join.children[0];//输入出节点
        this.InputNode = join.children[1];//输入节点
        this.InputNode.children.forEach(btn => btn.on(Input.EventType.TOUCH_START, this.click, this))
    }
    click({ target }: { target: Node }) {
        const val = target.children[0].getComponent(Label).string;
        // this.valArr.push(val)
        //转成数字存到valArr
        if (Number(val) || val === '0') {
            if (this.valArr.length < this.OutNode.children.length) {
                this.valArr.push(val)
            }
        }
        //删除数字
        if (target.name === KeyDownType.Delete) {
            this.valArr.pop()
        }
        //显示到outNode上面
        this.OutNode.children.forEach(item => item.children[0].getComponent(Label).string = '')
        this.OutNode.children.forEach((item, i) => item.children[0].getComponent(Label).string = this.valArr[i])
        //确认按钮
        if (target.name === KeyDownType.Confirm) {
            if (this.valArr.length < 6) return;
            Team.TeamData.roomid = this.valArr.join('');
            Team.TeamData.join = true;
            // new TeamData().saveData({ create: false, join: true, roomId: this.valArr.join('') });
            console.log(this.valArr.join(''));
            Loding.start_Loding("Game");
        }
    }
}