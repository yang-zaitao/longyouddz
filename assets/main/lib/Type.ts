//背包内容
export interface BagList {
  code: number;
  data: BagItem[];
  msg: string;
}

export interface BagItem {
  id: number;
  prop_id: number;
  prop_type: number;
  numbs: number;
  icon: string;
  name: string;
}
/**金币是否充足 */
export enum GoldAmple {
  /**充足 */
  Ample,
  /**不足 */
  noAmple
}