import { _decorator, AudioSource, Component, Node } from 'cc';
import { AudioConfig } from './StorageInfo';
const { ccclass, property } = _decorator;

@ccclass('Audio')
export class Audio extends Component {
    audioNode: Node[] = [];
    start() {
        // console.log('Audio音乐节点显示');
        this.audioNode = this.node.children;
        this.initAudio();
    }
    initAudio() {
        let bgmusic: AudioSource;
        let play: AudioSource;
        const Config = new AudioConfig().data;
        this.audioNode[0] && (bgmusic = this.audioNode[0].getComponent(AudioSource));
        this.audioNode[1] && (play = this.audioNode[1].getComponent(AudioSource));
        // console.log("Audio音效配置", Config);
        // console.log("Audio音乐节点", bgmusic, play);
        try {
            //音乐
            if (bgmusic !== undefined) {
                bgmusic.enabled = Config.musical;
                bgmusic.volume = Number(Config.volume.toFixed(2));
            }
            //音效
            if (play !== undefined) {
                play.enabled = Config.soundEffect;
                play.volume = Config.volume;
                console.log("Audio音效", play);
            }
        } catch (error) {
            console.log("读取音乐属性有错", Config);
        }
    }
}