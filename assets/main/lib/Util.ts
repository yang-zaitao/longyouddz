import { Input, Label, Node, Prefab, director, instantiate, resources } from 'cc';
import { Alert as Calert, Option } from '../common/Alert';
///////////////////////////
//游戏的工具类
//////////////////////////
enum pathType {
  alert = 'Prefab/Common/alert',
  tips = 'Prefab/Game/tips'
}
interface AlertArgs {

}
export namespace Util {
  /**弹窗类 */
  export class Alert {
    private canvas: Node = null;
    constructor() {
      this.canvas = director.getScene().getChildByName('Canvas');
    }
    /**显示弹窗 */
    ShowAlert(option: Option) {
      resources.load(pathType.alert, Prefab, (err, data) => {
        if (!err) {
          const alertNode = instantiate(data);
          const alert = alertNode.getComponent(Calert);
          alert.setData(option)
          this.canvas.addChild(alertNode);
        } else {
          console.log("弹窗预制体没找到");
        }
      })
    }
    /**GAME专用提示 */
    GameTips(msg: string = "手里的牌没有对面的大") {
      resources.load(pathType.tips, Prefab, (err, data) => {
        if (!err) {
          const alertNode = instantiate(data);
          alertNode.children[0].getComponent(Label).string = msg
          this.canvas.addChild(alertNode);
          setTimeout(() => {
            alertNode.removeFromParent();
          }, 1000)
        } else {
          console.log("tips预制体没找到");
        }
      })
    }
  }
  /**实现按钮组的toggle切换功能 */
  export class Toggle {
    private idx: number = 0;//指定tog节点下面第几个作为toggle节点
    //idx指定的是node下面第几个item
    //--node
    //----item1
    //----item2
    private btnkey: string = "";
    private bntArr: Node[] = null;//toggle按钮数组
    private centArr: Node[] = null;//内容节点数组
    constructor(btn: Node) {
      this.bntArr = btn.children;
    }

    /**指定tog下面第几个节点作为toggle子节点 */
    setBtnidx(idx: number) {
      this.idx = idx;
    }

    /**更具node的name,没有的设置一下nodename实现按钮toggle切换 */
    onTag(callbakfun: (suc: any) => void, btnidx: number = 0) {
      this.bntArr.forEach(btn => btn.children[this.idx].active = false);//隐藏所有的按钮
      this.bntArr[btnidx].children[this.idx].active = true;//显示一个被按下激活的按钮
      callbakfun(this.bntArr[btnidx].children[this.idx].name);//调用一次按钮的回调
      this.btnkey = this.bntArr[btnidx].children[this.idx].name;//储存当前按下的按钮
      this.bntArr.forEach(btn => {//绑定事件
        btn.off(Input.EventType.TOUCH_START);//先清空已经绑定事件
        btn.on(Input.EventType.TOUCH_START, () => {
          this.bntArr.forEach(btn => btn.children[this.idx].active = false);
          // console.log(this.btnkey);
          // console.log(btn.children[this.idx].name);
          if (this.btnkey !== btn.children[this.idx].name) {//如果当前按下的按钮不是上一次按的按钮
            callbakfun(btn.children[this.idx].name);
          }
          btn.children[this.idx].active = true;
          this.btnkey = btn.children[this.idx].name;//储存当前按下的按钮
        });
      });
    }

    /**可选  实现对应key显示对应内容node*/
    onCentent(cententNode: Node, key: string) {
      if (this.centArr === null) this.centArr = cententNode.children;
      this.centArr.forEach(cent => cent.active = false);
      this.centArr.find(cent => cent.name === key).active = true;
    }
  }

  /**格式化数值>=10000 转成 1w */
  export class Format {
    private fund: number;
    constructor(fund: string | number) {
      this.fund = !Number.isNaN(Number(fund)) && Number(fund);//不是数值就是flase
    }
    toString(char: string = 'w'): string {//格式化值为w
      let fund: string = '';
      if (typeof this.fund === 'number') {
        if (this.fund >= 10000) {//如果大于1w就格式化
          fund = `${(this.fund / 10000).toFixed(1).replace('.0', '')}${char}`;
        } else {//否则赋值原值
          fund = `${this.fund}`;
        }
        // console.log("要格式化的数值", this.fund);
      }
      return fund;
    }
    infinite() {
      if (this.toString() == '0') {
        return '无限'
      } else {
        return this.toString()
      }
    }
  }
}
/**用来打印有颜色标题log */
export class Log {
  private static print(type: 'success' | 'info' | 'error', msg: string, ...args: any[]) {
    const size = 2;
    const typeConfig: { [key: string]: any } = {
      'success': `padding:${size}px;background-color:green;color:white;border-radius:${size}px;`,
      'error': `padding:${size}px;background-color:red;color:white;border-radius:${size}px;`,
      'info': `padding:${size}px;background-color:orange;color:white;border-radius:${size}px;`,
    }
    console.log(`%c${msg}`, typeConfig[type], ...args);
  }
  public static success = (msg: string, ...args: any[]) => this.print('success', msg, ...args);
  public static error = (msg: string, ...args: any[]) => this.print('error', msg, ...args);
  public static info = (msg: string, ...args: any[]) => this.print('info', msg, ...args);
}