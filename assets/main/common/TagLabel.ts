import { _decorator, Component, Label, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('TagLabel')
export class TagLabel extends Component {
    @property(Label)
    label: Label = null;
    @property(Label)
    label2: Label = null;
    setLabel(label = '未按下', activelabel = '按下') {
        this.node.children[0].name = label;
        this.label.string = label;
        this.label2.string = activelabel;
    }
}

