import { _decorator, Component, director, Node } from 'cc';
import { Wechat } from '../Market/Wechat';
import { WECHAT } from 'cc/env';
import { Http, SucType } from '../lib/Net';
import { Util } from '../lib/Util';
import { BtnType } from './Alert';
import { Gamer } from '../lib/StorageInfo';
const { ccclass, property } = _decorator;

@ccclass('share')
export class share extends Component {
    @property(String)
    title: string = '';
    @property(String)
    imgurl: string = '';
    @property(String)
    args: string = '';
    click({ target }: { target: Node }) {
        if (this.title === '') {
            console.error('有分享按钮没设置标题:场景_', director.getScene().name);
        }
        // console.log(this.title);
        // console.log(this.imgurl);
        // console.log(this.args);
        new Http((res: SucType<{}>) => {
            if (res.code === 0) {
                setTimeout(() => {
                    new Util.Alert().ShowAlert({
                        msg: res.msg,
                        cancel: false,
                        confrm: true,
                        callfun: function (type: BtnType, node: Node): void {
                            node.destroy();
                        }
                    })
                }, 1000);
            }
        }).share(new Gamer().uid);
        if (WECHAT) {
            new Wechat().shar(this.title, this.imgurl, this.args);
            new Http((res: SucType<{}>) => {
                if (res.code === 0) {
                    setTimeout(() => {
                        new Util.Alert().ShowAlert({
                            msg: res.msg,
                            cancel: false,
                            confrm: true,
                            callfun: function (type: BtnType, node: Node): void {
                                node.destroy();
                            }
                        })
                    }, 1000);
                }
            }).share(new Gamer().uid);
        }
    }
}