import { _decorator, Component, Label } from 'cc';
import { Fund } from '../lib/StorageInfo';
const { ccclass, property } = _decorator;

@ccclass('fund')
export class fund extends Component {
    @property(Label)
    gold: Label = null;
    @property(Label)
    diamond: Label = null;
    static I: fund = null
    protected start() {
        fund.I = this;
    }
    protected onEnable() {
        this.init();
    }
    init() {
        //console.log('调用资金加载');
        new Fund(this.gold, this.diamond);
    }
    /**加载刷新资金 */
    static loadFund() {
        new Fund(this.I.gold, this.I.diamond);
    }
}

