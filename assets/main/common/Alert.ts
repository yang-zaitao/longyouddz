import { _decorator, Component, Label, Node } from 'cc';
const { ccclass, property } = _decorator;

export enum BtnType {
    /**取消 */
    Cancel = '取消',
    /**确定 */
    Confrm = '确定',
}

type callFun = (type: BtnType, node: Node) => void;
export type Option = {
    /**标题*/
    title?: string,
    /**消息内容*/
    msg: string,
    /**取消按钮*/
    cancel: boolean,
    /**确定按钮*/
    confrm: boolean,
    /**回调函数*/
    callfun: callFun
};

@ccclass('alert')
export class Alert extends Component {
    @property(Label)//标题文字
    Titie: Label;
    @property(Label)//内容文本
    Msg: Label;
    @property(Node)
    Btn: Node;
    private callfun: callFun;//按钮回调

    ondown({ target }: { target: Node }) {
        const node = target;
        switch (node.name) {
            case BtnType.Cancel: this.callfun(BtnType.Cancel, this.node); break;
            case BtnType.Confrm: this.callfun(BtnType.Confrm, this.node); break;
        }
    }
    /**设置消息以及回调函数 */
    setData(option: Option) {
        this.Btn.children[0].active = option.cancel;
        this.Btn.children[1].active = option.confrm;
        this.Titie.string = option.title;
        this.Msg.string = option.msg;
        this.callfun = option.callfun
        this.enabled = true;
    }
}

