// DO NOT EDIT! This is a generated file. Edit the JSDoc in src/*.js instead and run 'npm run build:types'.

/** Namespace com. */
export namespace com {

    /** Namespace handsome. */
    namespace handsome {

        /** Namespace landlords. */
        namespace landlords {

            /** Namespace entity. */
            namespace entity {

                /** Properties of a ClientTransferDataProtoc. */
                interface IClientTransferDataProtoc {

                    /** ClientTransferDataProtoc code */
                    code?: (string|null);

                    /** ClientTransferDataProtoc data */
                    data?: (string|null);

                    /** ClientTransferDataProtoc info */
                    info?: (string|null);
                }

                /** Represents a ClientTransferDataProtoc. */
                class ClientTransferDataProtoc implements IClientTransferDataProtoc {

                    /**
                     * Constructs a new ClientTransferDataProtoc.
                     * @param [properties] Properties to set
                     */
                    constructor(properties?: com.handsome.landlords.entity.IClientTransferDataProtoc);

                    /** ClientTransferDataProtoc code. */
                    public code: string;

                    /** ClientTransferDataProtoc data. */
                    public data: string;

                    /** ClientTransferDataProtoc info. */
                    public info: string;

                    /**
                     * Creates a new ClientTransferDataProtoc instance using the specified properties.
                     * @param [properties] Properties to set
                     * @returns ClientTransferDataProtoc instance
                     */
                    public static create(properties?: com.handsome.landlords.entity.IClientTransferDataProtoc): com.handsome.landlords.entity.ClientTransferDataProtoc;

                    /**
                     * Encodes the specified ClientTransferDataProtoc message. Does not implicitly {@link com.handsome.landlords.entity.ClientTransferDataProtoc.verify|verify} messages.
                     * @param message ClientTransferDataProtoc message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encode(message: com.handsome.landlords.entity.IClientTransferDataProtoc, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Encodes the specified ClientTransferDataProtoc message, length delimited. Does not implicitly {@link com.handsome.landlords.entity.ClientTransferDataProtoc.verify|verify} messages.
                     * @param message ClientTransferDataProtoc message or plain object to encode
                     * @param [writer] Writer to encode to
                     * @returns Writer
                     */
                    public static encodeDelimited(message: com.handsome.landlords.entity.IClientTransferDataProtoc, writer?: $protobuf.Writer): $protobuf.Writer;

                    /**
                     * Decodes a ClientTransferDataProtoc message from the specified reader or buffer.
                     * @param reader Reader or buffer to decode from
                     * @param [length] Message length if known beforehand
                     * @returns ClientTransferDataProtoc
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): com.handsome.landlords.entity.ClientTransferDataProtoc;

                    /**
                     * Decodes a ClientTransferDataProtoc message from the specified reader or buffer, length delimited.
                     * @param reader Reader or buffer to decode from
                     * @returns ClientTransferDataProtoc
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): com.handsome.landlords.entity.ClientTransferDataProtoc;

                    /**
                     * Verifies a ClientTransferDataProtoc message.
                     * @param message Plain object to verify
                     * @returns `null` if valid, otherwise the reason why it is not
                     */
                    public static verify(message: { [k: string]: any }): (string|null);

                    /**
                     * Creates a ClientTransferDataProtoc message from a plain object. Also converts values to their respective internal types.
                     * @param object Plain object
                     * @returns ClientTransferDataProtoc
                     */
                    public static fromObject(object: { [k: string]: any }): com.handsome.landlords.entity.ClientTransferDataProtoc;

                    /**
                     * Creates a plain object from a ClientTransferDataProtoc message. Also converts values to other types if specified.
                     * @param message ClientTransferDataProtoc
                     * @param [options] Conversion options
                     * @returns Plain object
                     */
                    public static toObject(message: com.handsome.landlords.entity.ClientTransferDataProtoc, options?: $protobuf.IConversionOptions): { [k: string]: any };

                    /**
                     * Converts this ClientTransferDataProtoc to JSON.
                     * @returns JSON object
                     */
                    public toJSON(): { [k: string]: any };

                    /**
                     * Gets the default type url for ClientTransferDataProtoc
                     * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
                     * @returns The default type url
                     */
                    public static getTypeUrl(typeUrlPrefix?: string): string;
                }
            }
        }
    }
}
