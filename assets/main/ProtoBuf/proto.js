/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/minimal")

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.com = (function() {

    /**
     * Namespace com.
     * @exports com
     * @namespace
     */
    var com = {};

    com.handsome = (function() {

        /**
         * Namespace handsome.
         * @memberof com
         * @namespace
         */
        var handsome = {};

        handsome.landlords = (function() {

            /**
             * Namespace landlords.
             * @memberof com.handsome
             * @namespace
             */
            var landlords = {};

            landlords.entity = (function() {

                /**
                 * Namespace entity.
                 * @memberof com.handsome.landlords
                 * @namespace
                 */
                var entity = {};

                entity.ClientTransferDataProtoc = (function() {

                    /**
                     * Properties of a ClientTransferDataProtoc.
                     * @memberof com.handsome.landlords.entity
                     * @interface IClientTransferDataProtoc
                     * @property {string|null} [code] ClientTransferDataProtoc code
                     * @property {string|null} [data] ClientTransferDataProtoc data
                     * @property {string|null} [info] ClientTransferDataProtoc info
                     */

                    /**
                     * Constructs a new ClientTransferDataProtoc.
                     * @memberof com.handsome.landlords.entity
                     * @classdesc Represents a ClientTransferDataProtoc.
                     * @implements IClientTransferDataProtoc
                     * @constructor
                     * @param {com.handsome.landlords.entity.IClientTransferDataProtoc=} [properties] Properties to set
                     */
                    function ClientTransferDataProtoc(properties) {
                        if (properties)
                            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                                if (properties[keys[i]] != null)
                                    this[keys[i]] = properties[keys[i]];
                    }

                    /**
                     * ClientTransferDataProtoc code.
                     * @member {string} code
                     * @memberof com.handsome.landlords.entity.ClientTransferDataProtoc
                     * @instance
                     */
                    ClientTransferDataProtoc.prototype.code = "";

                    /**
                     * ClientTransferDataProtoc data.
                     * @member {string} data
                     * @memberof com.handsome.landlords.entity.ClientTransferDataProtoc
                     * @instance
                     */
                    ClientTransferDataProtoc.prototype.data = "";

                    /**
                     * ClientTransferDataProtoc info.
                     * @member {string} info
                     * @memberof com.handsome.landlords.entity.ClientTransferDataProtoc
                     * @instance
                     */
                    ClientTransferDataProtoc.prototype.info = "";

                    /**
                     * Creates a new ClientTransferDataProtoc instance using the specified properties.
                     * @function create
                     * @memberof com.handsome.landlords.entity.ClientTransferDataProtoc
                     * @static
                     * @param {com.handsome.landlords.entity.IClientTransferDataProtoc=} [properties] Properties to set
                     * @returns {com.handsome.landlords.entity.ClientTransferDataProtoc} ClientTransferDataProtoc instance
                     */
                    ClientTransferDataProtoc.create = function create(properties) {
                        return new ClientTransferDataProtoc(properties);
                    };

                    /**
                     * Encodes the specified ClientTransferDataProtoc message. Does not implicitly {@link com.handsome.landlords.entity.ClientTransferDataProtoc.verify|verify} messages.
                     * @function encode
                     * @memberof com.handsome.landlords.entity.ClientTransferDataProtoc
                     * @static
                     * @param {com.handsome.landlords.entity.IClientTransferDataProtoc} message ClientTransferDataProtoc message or plain object to encode
                     * @param {$protobuf.Writer} [writer] Writer to encode to
                     * @returns {$protobuf.Writer} Writer
                     */
                    ClientTransferDataProtoc.encode = function encode(message, writer) {
                        if (!writer)
                            writer = $Writer.create();
                        if (message.code != null && Object.hasOwnProperty.call(message, "code"))
                            writer.uint32(/* id 1, wireType 2 =*/10).string(message.code);
                        if (message.data != null && Object.hasOwnProperty.call(message, "data"))
                            writer.uint32(/* id 2, wireType 2 =*/18).string(message.data);
                        if (message.info != null && Object.hasOwnProperty.call(message, "info"))
                            writer.uint32(/* id 3, wireType 2 =*/26).string(message.info);
                        return writer;
                    };

                    /**
                     * Encodes the specified ClientTransferDataProtoc message, length delimited. Does not implicitly {@link com.handsome.landlords.entity.ClientTransferDataProtoc.verify|verify} messages.
                     * @function encodeDelimited
                     * @memberof com.handsome.landlords.entity.ClientTransferDataProtoc
                     * @static
                     * @param {com.handsome.landlords.entity.IClientTransferDataProtoc} message ClientTransferDataProtoc message or plain object to encode
                     * @param {$protobuf.Writer} [writer] Writer to encode to
                     * @returns {$protobuf.Writer} Writer
                     */
                    ClientTransferDataProtoc.encodeDelimited = function encodeDelimited(message, writer) {
                        return this.encode(message, writer).ldelim();
                    };

                    /**
                     * Decodes a ClientTransferDataProtoc message from the specified reader or buffer.
                     * @function decode
                     * @memberof com.handsome.landlords.entity.ClientTransferDataProtoc
                     * @static
                     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                     * @param {number} [length] Message length if known beforehand
                     * @returns {com.handsome.landlords.entity.ClientTransferDataProtoc} ClientTransferDataProtoc
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    ClientTransferDataProtoc.decode = function decode(reader, length) {
                        if (!(reader instanceof $Reader))
                            reader = $Reader.create(reader);
                        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.com.handsome.landlords.entity.ClientTransferDataProtoc();
                        while (reader.pos < end) {
                            var tag = reader.uint32();
                            switch (tag >>> 3) {
                            case 1: {
                                    message.code = reader.string();
                                    break;
                                }
                            case 2: {
                                    message.data = reader.string();
                                    break;
                                }
                            case 3: {
                                    message.info = reader.string();
                                    break;
                                }
                            default:
                                reader.skipType(tag & 7);
                                break;
                            }
                        }
                        return message;
                    };

                    /**
                     * Decodes a ClientTransferDataProtoc message from the specified reader or buffer, length delimited.
                     * @function decodeDelimited
                     * @memberof com.handsome.landlords.entity.ClientTransferDataProtoc
                     * @static
                     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                     * @returns {com.handsome.landlords.entity.ClientTransferDataProtoc} ClientTransferDataProtoc
                     * @throws {Error} If the payload is not a reader or valid buffer
                     * @throws {$protobuf.util.ProtocolError} If required fields are missing
                     */
                    ClientTransferDataProtoc.decodeDelimited = function decodeDelimited(reader) {
                        if (!(reader instanceof $Reader))
                            reader = new $Reader(reader);
                        return this.decode(reader, reader.uint32());
                    };

                    /**
                     * Verifies a ClientTransferDataProtoc message.
                     * @function verify
                     * @memberof com.handsome.landlords.entity.ClientTransferDataProtoc
                     * @static
                     * @param {Object.<string,*>} message Plain object to verify
                     * @returns {string|null} `null` if valid, otherwise the reason why it is not
                     */
                    ClientTransferDataProtoc.verify = function verify(message) {
                        if (typeof message !== "object" || message === null)
                            return "object expected";
                        if (message.code != null && message.hasOwnProperty("code"))
                            if (!$util.isString(message.code))
                                return "code: string expected";
                        if (message.data != null && message.hasOwnProperty("data"))
                            if (!$util.isString(message.data))
                                return "data: string expected";
                        if (message.info != null && message.hasOwnProperty("info"))
                            if (!$util.isString(message.info))
                                return "info: string expected";
                        return null;
                    };

                    /**
                     * Creates a ClientTransferDataProtoc message from a plain object. Also converts values to their respective internal types.
                     * @function fromObject
                     * @memberof com.handsome.landlords.entity.ClientTransferDataProtoc
                     * @static
                     * @param {Object.<string,*>} object Plain object
                     * @returns {com.handsome.landlords.entity.ClientTransferDataProtoc} ClientTransferDataProtoc
                     */
                    ClientTransferDataProtoc.fromObject = function fromObject(object) {
                        if (object instanceof $root.com.handsome.landlords.entity.ClientTransferDataProtoc)
                            return object;
                        var message = new $root.com.handsome.landlords.entity.ClientTransferDataProtoc();
                        if (object.code != null)
                            message.code = String(object.code);
                        if (object.data != null)
                            message.data = String(object.data);
                        if (object.info != null)
                            message.info = String(object.info);
                        return message;
                    };

                    /**
                     * Creates a plain object from a ClientTransferDataProtoc message. Also converts values to other types if specified.
                     * @function toObject
                     * @memberof com.handsome.landlords.entity.ClientTransferDataProtoc
                     * @static
                     * @param {com.handsome.landlords.entity.ClientTransferDataProtoc} message ClientTransferDataProtoc
                     * @param {$protobuf.IConversionOptions} [options] Conversion options
                     * @returns {Object.<string,*>} Plain object
                     */
                    ClientTransferDataProtoc.toObject = function toObject(message, options) {
                        if (!options)
                            options = {};
                        var object = {};
                        if (options.defaults) {
                            object.code = "";
                            object.data = "";
                            object.info = "";
                        }
                        if (message.code != null && message.hasOwnProperty("code"))
                            object.code = message.code;
                        if (message.data != null && message.hasOwnProperty("data"))
                            object.data = message.data;
                        if (message.info != null && message.hasOwnProperty("info"))
                            object.info = message.info;
                        return object;
                    };

                    /**
                     * Converts this ClientTransferDataProtoc to JSON.
                     * @function toJSON
                     * @memberof com.handsome.landlords.entity.ClientTransferDataProtoc
                     * @instance
                     * @returns {Object.<string,*>} JSON object
                     */
                    ClientTransferDataProtoc.prototype.toJSON = function toJSON() {
                        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
                    };

                    /**
                     * Gets the default type url for ClientTransferDataProtoc
                     * @function getTypeUrl
                     * @memberof com.handsome.landlords.entity.ClientTransferDataProtoc
                     * @static
                     * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
                     * @returns {string} The default type url
                     */
                    ClientTransferDataProtoc.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
                        if (typeUrlPrefix === undefined) {
                            typeUrlPrefix = "type.googleapis.com";
                        }
                        return typeUrlPrefix + "/com.handsome.landlords.entity.ClientTransferDataProtoc";
                    };

                    return ClientTransferDataProtoc;
                })();

                return entity;
            })();

            return landlords;
        })();

        return handsome;
    })();

    return com;
})();

module.exports = $root;
