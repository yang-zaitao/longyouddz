import { _decorator, assetManager, Component, director, Label, ProgressBar, Scene, SceneAsset, Node } from 'cc';
import { Log } from '../lib/Util';
const { ccclass, type } = _decorator;

@ccclass('Loding')
export class Loding extends Component {
    /**进度条节点*/@type(ProgressBar)
    ProgressBar: ProgressBar = null;
    /**提示文本节点 */@type(Label)
    MessageLabel: Label = null;
    protected start(): void {
        Loding.I = this;
        new LoadBunld(this.ProgressBar, Loding.bundle_name).start();
        new sendMsg(this.MessageLabel, Loding.msg_arr).start();//要不要在登录界面显示循环消息
    }
    /**要加载场景的name */
    private static bundle_name: string = "";
    private static msg_arr: string[] = [];
    public static I: Loding = null;
    /**
     * 启动加载场景
     * @param bundle_name 要加载的bundle_name
     * @param msg_arr 消息数组
    */
    static start_Loding(bundle_name: "Main_" | "Game", msg_arr?: string[]) {
        Loding.bundle_name = bundle_name;
        Loding.msg_arr = msg_arr;
        director.loadScene("Loding");
    }
    /**login登录首页专用 */
    static load_home(progressNode: Node, msg_arr: string[] = ["一句话", "太好玩了", "人人都爱玩的"]) {
        const proBar = progressNode.getComponent(ProgressBar);
        const proMsg = progressNode.children[2].getComponent(Label);
        new LoadBunld(proBar, "Main_").start();
        new sendMsg(proMsg, msg_arr).start();
    }
}
/**加载指定budlne资源文件夹且加载指定scene场景 */
class LoadBunld {
    SceneMap: { [key: string]: string } = { Main_: "Main", Game: "Game" };
    ProgressBar: ProgressBar = null;
    BundleName: string = "";
    constructor(progressbar: ProgressBar, bundle_name: string) {
        this.ProgressBar = progressbar;
        this.BundleName = bundle_name;
        // console.log("包名", this.BundleName);
    }
    start() {
        // try {
        //     assetManager.cacheManager.removeCache(this.BundleName);
        // } catch (error) {
        //     Log.info(`途径为${this.BundleName}暂时没有缓存可清除!`);
        // }
        //这里先加载asstMan分包资源
        assetManager.loadBundle(this.BundleName, (err, bundle) => {
            if (err === null) {
                bundle.loadScene(this.SceneMap[this.BundleName], onProgress, onComplete);
            } else {
                Log.error("资源加载错误:", err);
            }
        });

        /**加载中----设置进度条 */
        const onProgress = (finished: number, total: number) => {
            this.ProgressBar.progress = finished / total;
        }

        /**加载完成-----打开场景 */
        const onComplete = (err: any, data: { scene: Scene | SceneAsset; }) => {
            if (err === undefined) {
                director.runScene(data.scene);
            } else {
                Log.error('没有在分包里找到场景文件', data);
            }
        }
    }
}
/**给指定Labe节点循环设置string */
class sendMsg {
    Promsg: Label = null;
    MsgArr: string[] = [];
    constructor(promsg: Label, msg_arr: string[] = []) {
        this.Promsg = promsg;
        this.MsgArr = msg_arr;
    }
    start() {
        for (let i = 0; i < this.MsgArr.length; i++) {
            setTimeout(() => {
                this.Promsg.string = this.MsgArr.sort(() => Math.random() - 0.5)[0];
            }, (i + 1) * 1000);
        }
    }
}