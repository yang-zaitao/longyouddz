import { _decorator, Component, Label, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Cliock')
export class Cliock extends Component {
    @property(Node)
    cliock: Node = null;
    @property(Node)
    oblate: Node = null;
    LabelNode: Label = null;
    static countTime: number = 30;
    start() {
        this.LabelNode = this.cliock.children[0].getComponent(Label);
        this.LabelNode.string = String(Cliock.countTime);//显示文字
        this.schedule(this.timefun, 1, Cliock.countTime);//开始倒计时
    }
    timefun() {
        if (Cliock.countTime > 0) {
            Cliock.countTime -= 1;
            this.LabelNode.string = String(Cliock.countTime);
        } else {
            this.unschedule(this.timefun);
            this.destroy();
        }
    }
    protected onDestroy(): void {
        this.unschedule(this.timefun);
    }
}

