import { _decorator, Component, EventTouch, Input, Label, Node } from 'cc';
import { Http } from '../../main/lib/Net';
import { Util } from '../../main/lib/Util';
import { Gamer } from '../../main/lib/StorageInfo';
const { ccclass, property } = _decorator;

@ccclass('Folter')
export class Folter extends Component {
    /**金币节点 */
    @property(Label)
    Gold: Label = null;
    /**倍数节点 */
    @property(Label)
    fold: Label = null;
    /**聊天节点 */
    @property(Node)
    chatNode: Node = null;
    static I: Folter = null;
    start() {
        Folter.I = this;
        this.chatNode.on(Input.EventType.TOUCH_START, this.chatdown, this);
        new Http<any>(suc => {//请求用户数据
            console.log("请求用户数据", suc);
            this.Gold.string = new Util.Format(suc.data.coin).toString();//设置金币
        }).gamerinfo({ source: "", uid: new Gamer().uid.toString() });
    }
    chatdown() {
        const chatNode_ = this.chatNode.children[0];
        const msgArr = chatNode_.children[1].children[0].children;//文字按钮节点数组
        chatNode_.active = !chatNode_.active;//toggle显示消息列表
        console.log("点击了消息按钮");
        msgArr.forEach(msgNode => {
            msgNode.off(Input.EventType.TOUCH_START);//先清空点击事件
            msgNode.on(Input.EventType.TOUCH_START, (ent: EventTouch) => {
                ent.propagationStopped = true;//禁用事件冒泡
                // chatNode_.active = false;//隐藏消息列表
                console.log("发送的消息为:", msgNode.children[0].getComponent(Label).string);
            }, this);
        });
    }
    setGold(val: number) {
        this.Gold.string = new Util.Format(val).toString();//设置金币
    }
    setFold(val: number) {
        this.fold.string = val.toString();
    }
}

