import { _decorator, Component, Label, Node, sys } from 'cc'
import { Button, Game } from '../Game'
import { Loding } from '../../main/Loding/Loding'
import { Util } from '../../main/lib/Util'
import { BtnType as BtnTypeA } from '../../main/common/Alert'
const { ccclass, property } = _decorator

@ccclass('Header')
export class Header extends Component {
  // @property(Game)
  // game: Game = null
  /**Labe时间节点 */ @property(Label)
  label: Label = null
  static GAME_OVER: boolean = true;
  start() {
    this.init()
  }
  init() {
    sys.getBatteryLevel() 
    this.schedule(() => {//每秒设置时间
      this.label.string = `${new Date().toLocaleDateString()}\n${new Date().toLocaleTimeString()}`
    }, 1)
  }
  ondown({ target }: { target: Node }) {
    // console.log(target)
    switch (target.name) {
      case BtnType.Exit:
        console.log(Game.I.Escrow.active);
        if (Header.GAME_OVER) {//已经在结算
          new Util.Alert().ShowAlert({
            msg: '是否退出游戏?',
            cancel: true,
            confrm: true,
            callfun: (arg: BtnTypeA, node) => {
              if (arg === BtnTypeA.Confrm) {
                Game.I.ws?.close();//关闭ws连接
                Loding.start_Loding('Main_')
              }
              node.destroy();
            }
          })
        } else {
          new Util.Alert().ShowAlert({
            msg: '游戏未结束不能退出',
            cancel: false,
            confrm: true,
            callfun: (arg, node) => {
              node.destroy();
            }
          })
        }
        break;
      case BtnType.Cardholder:
        break;
      case BtnType.Transform:
        Game.I.ws.change_trustee(true);
        new Button(false)
        break;
    }
  }
}

enum BtnType {
  /**退出房间 */
  Exit = '退出房间',
  /**记牌器 */
  Cardholder = '记牌器',
  /**托管 */
  Transform = '托管',
  /**更多 */
}
