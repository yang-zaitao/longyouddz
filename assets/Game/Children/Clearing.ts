import { _decorator, Component, Input, instantiate, Label, Node, Prefab } from 'cc'
import { Loding } from '../../main/Loding/Loding'
import { GameOverType, Http, SucType } from '../../main/lib/Net';
import { Gamer, RoomInfo } from '../../main/lib/StorageInfo';
import { Util } from '../../main/lib/Util';
import { Game } from '../Game';
import { Wechat } from '../../main/Market/Wechat';
import { BtnType } from '../../main/common/Alert';
const { ccclass, property } = _decorator

@ccclass('Clearing')
export class Clearing extends Component {
  //结果节点输赢面板
  @property(Node)
  resultsNode: Node = null;
  @property(Node)
  resultsDataNode: Node = null;
  //玩家名|低分|倍数|金币 节点
  @property([Node])
  fundS: Node[] = [];
  //文字预制体
  @property(Prefab)
  label: Prefab = null;
  //游戏结束数据
  static overData: GameOverType = null;
  onEnable() {
    console.log("游戏结束数据", Clearing.overData);
    const target = this.node.children[0];//长按显示牌桌文字节点
    target.on(Input.EventType.TOUCH_START, () => {//长按显示牌桌
      this.node.children[1].active = false;
      target.on(Input.EventType.TOUCH_END, () => this.node.children[1].active = true);
      target.on(Input.EventType.TOUCH_CANCEL, () => this.node.children[1].active = true);
    });
    try {
      new Results(this.resultsNode, Clearing.overData).computeData(this.fundS, this.label);
    } catch (error) {
      console.error("Game里面的clearing节点在引擎控制面板里面没有隐藏！！！");
    }
    // new Results(this.resultsNode).computeData(this.fundS, this.label);
  }
  ondown({ target }: { target: Node }) {
    switch (target.name) {
      case BtnType2.ExitGame:
        Loding.start_Loding('Main_', ['打的不错', '这局牌也太好了']);
        Game.I.ws.exitRoom()
        break;
      case BtnType2.ContinueGame:
        console.log("按下了继续游戏");
        if (new RoomInfo().type === 3) {
          Game.I.ws.startGame()
          this.node.active = false;
        } else {
          Loding.start_Loding("Game");
        }
        break;
      case BtnType2.Share:
        try {
          new Wechat().shar();
        } catch (error) { }
        new Http((res: SucType<{}>) => {
          console.log(res);
          if (res.code === 0) {
            new Util.Alert().ShowAlert({
              msg: res.msg,
              cancel: false,
              confrm: true,
              callfun: function (type: BtnType, node: Node): void {
                Loding.start_Loding('Main_', ['打的不错', '这局牌也太好了']);
                Game.I.ws.exitRoom();
              }
            })
          }
        }).share(new Gamer().uid);
        break;
      case BtnType2.ViewTale:
        console.log("长按");
        break;
    }
  }
}

enum BtnType2 {
  ExitGame = '返回大厅',
  ContinueGame = '继续游戏',
  ViewTale = '长按查看牌桌',
  Share = '分享得金币'
}

/**显示结果面板 */
class Results {
  resNode: Node;
  resData: GameOverType;
  constructor(resnode: Node, resdata: GameOverType) {
    this.resNode = resnode;//胜利失败节点
    this.resData = resdata;//结算数据
    this.compute();
  }
  compute() {
    let bol = false;
    const myData = this.resData.player.filter(gamer => gamer.id === new Gamer().uid)[0];//更具自己的uid节点拿到对应的数据
    if (myData.type === this.resData.winnerType) bol = true;//如果胜利bol设置true
    this.resNode.children[0].active = bol;//胜利节点
    this.resNode.children[1].active = !bol;//失败节点
  }
  computeData(funds: Node[], label: Prefab) {
    console.log("结算数据面板", funds);
    funds.forEach(node => node.removeAllChildren());
    this.resData.player.forEach((gamerData, idx) => {
      const name = instantiate(label);//玩家名
      name.getComponent(Label).string = gamerData.nickName;
      funds[0].addChild(name);

      const scroe = instantiate(label);//金币
      // scroe.getComponent(Label).string = gamerData.fan.toString();
      scroe.getComponent(Label).string = new Util.Format(gamerData.fan).toString()
      if (gamerData.fan === 0) {
        scroe.getComponent(Label).string = `直接破产`;
      }
      funds[1].addChild(scroe);

      const fold = instantiate(label);//倍数
      fold.getComponent(Label).string = new Util.Format(gamerData.score).toString();
      funds[2].addChild(fold);

      const scoreInc = instantiate(label);//结算
      if (gamerData.scoreInc > 0) {
        scoreInc.getComponent(Label).string = `+${new Util.Format(gamerData.scoreInc).toString()}`;
      } else {
        scoreInc.getComponent(Label).string = `-${new Util.Format(String(gamerData.scoreInc).replace('-', '')).toString()}`;
      }
      funds[3].addChild(scoreInc);
    });
  }
}