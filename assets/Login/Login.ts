import { _decorator, Component, Input, Node, sys } from 'cc';
import { HTML5, WECHAT } from 'cc/env';
import { Loding } from '../main/Loding/Loding';
import { Http, Socket, SucType } from '../main/lib/Net';
import { Log, Util } from '../main/lib/Util';
import { Gamer } from '../main/lib/StorageInfo';
import { Wechat } from '../main/Market/Wechat';
import { BtnType as Btntype } from '../main/common/Alert';
const { ccclass, type } = _decorator;

@ccclass('Login')
export class Login extends Component {
    @type(Node)
    ProgressBar: Node = null;//进度条
    @type(Node)
    Pop_ups: Node = null;//隐私保护
    static I: Login = null;
    start(): void {
        Login.I = this;
        //循环btns子节点添加事件
        this.Pop_ups.getChildByName("Btns").children.forEach(btn => btn.on(Input.EventType.TOUCH_START, this.ondown, this));
        const env = new Wechat().getEnv();//当前环境
        const version = '1.0.3';//程序版本
        let program_env = 'web';//当前运行程序的系统
        if (WECHAT) program_env = 'wechat';//如果是微信小程序
        //请求服务器拿取http和socket配置
        new Http((res: SucType<{ http: string, socket: string }>) => {
            // console.log(res);
            if (res?.code === 0) {
                Http.URL = res.data.http;
                Socket.URL = res.data.socket;
                console.log('当前服务器环境为', { env, version, program_env }, res.data);
            } else {
                new Util.Alert().ShowAlert({
                    msg: '服务器错误！',
                    cancel: false,
                    confrm: true,
                    callfun: function (type: Btntype, node: Node): void {
                        node.destroy();
                        if (WECHAT) {
                            wx.exitMiniProgram();
                        }
                    }
                })
            }
        }).loadURLS(env)
        //如果是微信环境
        if (WECHAT) {
            wx.showShareMenu({ //设置分享朋友和朋友圈
                withShareTicket: true,
                menus: ['shareAppMessage', 'shareTimeline']
            })
            console.log(wx.getLaunchOptionsSync());
            this.Pop_ups.active = false;
            new Wechat().fastLogin(res => {
                console.log("快速登陆数据", res);
                if (res.success) {
                    new Prompt(BtnType.none).hide();//进入游戏界面
                } else {
                    this.Pop_ups.active = true;
                }
            })
        }
    }
    ondown({ target }: { target: Node }) {
        const btn_node: Node = target;
        switch (btn_node.name) {
            case '同意': new Prompt(BtnType.Agree); break;
            case '拒绝': new Prompt(BtnType.Rejected); break;
        }
    }
}

enum BtnType {
    /**同意 */
    Agree,
    /**拒绝 */
    Rejected,
    /**啥都不做 */
    none
}

/**同意 | 拒绝 */
class Prompt { //按钮点击监听class
    Pop_ups: Node = Login.I.Pop_ups;
    uname: string;//url上的用户名
    constructor(btnType: BtnType = BtnType.none) {
        switch (btnType) {
            case BtnType.Agree:

                if (HTML5) {//h5平台
                    // console.log("h5");
                    const params = new URLSearchParams(window.location.search);
                    this.uname = params.get('name');
                    if (this.uname !== null) {
                        new Http(suc => {
                            new Gamer().saveData(suc);
                            this.hide();
                        }).loginweb({ accName: this.uname });
                    } else {
                        Log.error("url需要一个name参数!!!");
                    }
                }
                if (WECHAT) {//微信小程序平台
                    // console.log('微信登录');
                    new Wechat().Login2(res => {
                        console.log("弹窗登录", res);
                        this.hide();//进入游戏界面
                    });
                }
                break;
            case BtnType.Rejected:
                if (HTML5) {//h5平台

                }
                if (WECHAT) {//微信小程序平台
                    wx.exitMiniProgram();
                }
                break;
        }
    }
    /**隐藏隐私声明直接进入游戏 */
    hide() {
        this.Pop_ups.active = false;
        Login.I.ProgressBar.active = true;
        Loding.load_home(Login.I.ProgressBar, []);
        // new Progress("Main_","Main")
    }
}